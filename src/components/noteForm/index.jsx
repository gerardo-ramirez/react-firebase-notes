import React, { Component } from 'react';
import './noteForm.css';

class NoteForm extends Component{
  constructor(){
    super();
     //this.textInput = React.createRef()
  }

  addNote = ()=>{
    let data = document.getElementById('input').value;
    this.props.addNote(data);
    data ='';
  
    document.getElementById('input').focus();

  };
  render(){
    return(
      <div className='NoteForm'>
      <input type = 'text' placeholder='escribe una nota' id='input' />
      <button onClick={this.addNote}>Add Note</button>
      </div>

    )
  }
};
export default NoteForm ;
