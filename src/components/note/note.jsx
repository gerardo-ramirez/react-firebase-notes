import React, { Component } from 'react';
import './note.css';
class Note extends Component {
  constructor(props){
    super(props);
     this.noteId = props.noteId;
     this.noteContent =  props.noteContent;
  };

  handleRemove = (id)=>{
    const response = window.confirm('estas seguro de eliminar');
    if(response){
      this.props.removeNote(id);
    }
    return;
  };
  render(){
    return(
      <div className='note'>
      <spam
      onClick={()=>this.handleRemove(this.noteId)} >&times;</spam>
      <h5> {this.noteId}</h5>
      <p>  {this.noteContent} </p>
      </div>
    )
  }

}
export default Note ;
