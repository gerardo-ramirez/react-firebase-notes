import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';
import Note from './components/note/note';
import NoteForm from './components/noteForm';
//importamos base firebase
import firebase from 'firebase';
import 'firebase/database';
import { DB_CONFIG } from './config/config'

class App extends Component {
  constructor(){
    super();
    this.state = {
      notes: [
        {noteId: 1, noteContent: 'note1'},
        {noteId: 2, noteContent: 'note2'},

      ]
    };
    this.app = firebase.initializeApp(DB_CONFIG);
    this.db = this.app.database().ref().child('notes');
  };

  componentDidMount(){
    const notes =   this.state.notes;
    this.db.on('child_added', snap => {
      notes.push({
        noteId: snap.key,
        noteContent: snap.val().noteContent
      });
      this.setState({notes})


    }); this.db.on('child_removed', snap =>{
      for(let i = 0 ; i<notes.length ; i++ ){
        if(notes[i].noteId = snap.key){
          notes.splice(i,1);
        }
      }
      this.setState({notes})
    })
  }
  //funcion para el hijo CUANDO ESTE NOS DEVUELVA ESTA FUNCION
  removeNote = (noteId) =>{
      this.db.child(noteId).remove();

  };
  addNote = (note)=> {
    let { notes } = this.state ;
    notes.push({
      noteContent : note,
      noteId: notes.length + 1
    });
    this.setState({
      notes
    });
    this.db.push().set({noteContent: note});
  };
  render(){
    return (
      <div className="notesContainer">
        <div className="noteHeader">
          <h1>React y farebase app</h1>
        </div>
        <div className='noteBody'>
        <ul>
        {this.state.notes.map(note => {
          return (
            <Note Key={note.noteId}
                noteContent = {note.noteContent}
                noteId = {note.noteId}
                removeNote={this.removeNote } 
            />
          )
        })}
        </ul>

        </div>
        <div className='noteFooter'>
          <NoteForm addNote = {this.addNote}/>
        </div>

      </div>
    )

  }

};

export default App;
